package net.chenlin.dp.ids.server;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * passport启动器
 * @author zcl<yczclcn@163.com>
 */
@SpringBootApplication
@MapperScan("net.chenlin.dp.ids.server.dao")
public class PassportApplication {

    private static final Logger logger = LoggerFactory.getLogger(PassportApplication.class);

    /**
     * jar启动
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(PassportApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
        logger.info("The dp passport has been started successfully!");
    }

}
